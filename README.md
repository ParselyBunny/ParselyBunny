# Meet Parsely

Hi! I'm a non-binary, queer, neurodivergent game developer. Pronouns: they/them, she/her.

I currently contribute to several game projects:
## [An Outcry](https://quinnk.itch.io/outcry)
A surreal, horror RPG with branching paths by GlasStadtGames.

**Role**: Event scripting

[![An Outcry trailer](https://raw.githubusercontent.com/ParselyBunny/ParselyBunny/master/images/an_outcry_thumbnail.png)](https://www.youtube.com/watch?v=8UvLNnXDsY0)


## World of Friends
A cute, 2.5d RPG with paper doll style animation developed in Unity 3D and inspired by Paper Mario: The Thousand Year Door.

**Role**: Unity developer

![A 2D character walks around a 3D level, approaching various NPCs.](https://raw.githubusercontent.com/ParselyBunny/ParselyBunny/master/images/world_of_friends_demo.gif)


## Painted Shadows
A free, horror visual novel with branching paths, developed in Renpy by a majority LGBT+ team.

**Role**: Lead programmer

![Several scenes of 2D characters in a modern setting having conversations. There are themes of violence and anger throughout the dialogue.](https://raw.githubusercontent.com/ParselyBunny/ParselyBunny/master/images/painted_shadows_demo.gif)
